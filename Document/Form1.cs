﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Document.Form1;

namespace Document
{
    public partial class Form1 : Form
    {
        private Stack<Error> erros = new Stack<Error>();

        public Form1()
        {
            InitializeComponent();
            StartView();        
        }

        private void StartView()
        {
            listView1.View = View.Details;
            listView1.Columns.Add("Mensagem de texto:", -2, HorizontalAlignment.Right);
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            listView1.Items.Add("Clique aqui 3x para adicionar um texto:");
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            listView1.View = View.Details;

            if (listView1.Items.Count > 0)
                listView1.Items.RemoveAt(listView1.Items.Count - 1);
        }

        private void btnExibir_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Error erro in erros)
            {
                sb.AppendLine($"[{erro.Horario}] {erro.Mensagem}");
            }
            MessageBox.Show(sb.ToString(), "Registro de erro", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Arquivo de texto|*.txt";
                saveFileDialog1.Title = "Salvar arquivo de texto";
                saveFileDialog1.ShowDialog();

                if (saveFileDialog1.FileName != "")
                {
                    using (StreamWriter sw = new StreamWriter(saveFileDialog1.FileName))
                    {
                        foreach (ListViewItem item in listView1.Items)
                        {
                            sw.WriteLine(item.Text);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                erros.Push(new Error(ex.Message, DateTime.Now));
            }
        }

        private void btnImportar_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.Filter = "Arquivo de texto|*.txt";
                openFileDialog1.Title = "Selecione o arquivo de texto";
                openFileDialog1.ShowDialog();

                if (openFileDialog1.FileName != "")
                {
                    listView1.Items.Clear();
                    using (StreamReader sr = new StreamReader(openFileDialog1.FileName))
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            string[] parts = line.Split(',');
                            listView1.Items.Add(new ListViewItem(parts));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                erros.Push(new Error(ex.Message, DateTime.Now));
            }
        }
    }
}

